'use strict';

/**
 *  about-us-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

//module.exports = createCoreController('api::about-us-page.about-us-page');

module.exports = createCoreController('api::about-us-page.about-us-page', ({ strapi }) => ({
    async find(ctx) {
        const { query } = ctx;

        const entity = await strapi.entityService.findMany('api::about-us-page.about-us-page', {
            ...query,
            populate: {
                BgImage: {
                    populate: {
                        attributes: true
                    }
                },
                AboutUs: {
                    populate: {
                        Image: true
                    }
                },
                PromoImageOne: {
                    populate: {
                        attributes: true
                    }
                },
                PromoImageTwo: {
                    populate: {
                        attributes: true
                    }
                },
                PromoImageThree: {
                    populate: {
                        attributes: true
                    }
                },
                Team: {
                    populate: {
                        Image: true
                    }
                }
            },
        });
        const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

        return this.transformResponse(sanitizedEntity);

    }
}));
