'use strict';

/**
 *  campaign-order controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::campaign-order.campaign-order');
