'use strict';

/**
 * campaign-order router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::campaign-order.campaign-order');
