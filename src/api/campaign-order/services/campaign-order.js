'use strict';

/**
 * campaign-order service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::campaign-order.campaign-order');
