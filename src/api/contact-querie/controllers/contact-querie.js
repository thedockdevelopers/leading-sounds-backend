'use strict';

/**
 *  contact-querie controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::contact-querie.contact-querie');
