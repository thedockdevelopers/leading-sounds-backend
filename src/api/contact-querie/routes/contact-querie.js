'use strict';

/**
 * contact-querie router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::contact-querie.contact-querie');
