'use strict';

/**
 * contact-querie service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::contact-querie.contact-querie');
