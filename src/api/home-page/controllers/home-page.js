'use strict';

/**
 *  home-page controller
 */

 const { createCoreController } = require('@strapi/strapi').factories;

// module.exports = createCoreController('api::home-page.home-page');


module.exports = createCoreController('api::home-page.home-page', ({ strapi }) => ({
    async find(ctx) {
        const { query } = ctx;

        const entity = await strapi.entityService.findMany('api::home-page.home-page', {
            ...query,
            populate: {
                Teasers: {
                    populate: {
                        Image: true
                    }
                },
                AboutUs: {
                    populate: {
                        Image: true
                    }
                },
                Partners: {
                    populate: {
                        PartnerLogo: true
                    }
                },
                Services: {
                    populate: {
                        Image: true
                    }
                },
                Testimonials: {
                    populate: {
                        Image: true
                    }
                },
                WhyPromote: {
                    populate: {
                        attributes: true
                    }
                },
                BeforeImage: {
                    populate: {
                        attributes: true
                    }
                },
                AfterImage: {
                    populate: {
                        attributes: true
                    }
                },
                ServiceBgImage: {
                    populate: {
                        attributes: true
                    }
                },
                Video: {
                    populate: {
                        attributes: true
                    }
                }
            },
        });
        const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

        return this.transformResponse(sanitizedEntity);

    }
}));