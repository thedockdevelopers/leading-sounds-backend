'use strict';

/**
 *  instagram-campaign-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::instagram-campaign-page.instagram-campaign-page');
