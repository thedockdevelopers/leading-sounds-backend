'use strict';

/**
 * instagram-campaign-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::instagram-campaign-page.instagram-campaign-page');
