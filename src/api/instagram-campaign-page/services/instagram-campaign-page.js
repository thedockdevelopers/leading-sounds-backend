'use strict';

/**
 * instagram-campaign-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::instagram-campaign-page.instagram-campaign-page');
