'use strict';

/**
 *  instagram-package controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::instagram-package.instagram-package');
