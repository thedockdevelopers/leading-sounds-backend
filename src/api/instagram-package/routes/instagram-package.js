'use strict';

/**
 * instagram-package router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::instagram-package.instagram-package');
