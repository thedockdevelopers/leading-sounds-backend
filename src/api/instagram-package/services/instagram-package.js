'use strict';

/**
 * instagram-package service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::instagram-package.instagram-package');
