'use strict';

/**
 *  instagram-repost-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

//module.exports = createCoreController('api::instagram-repost-page.instagram-repost-page');

module.exports = createCoreController('api::instagram-repost-page.instagram-repost-page', ({ strapi }) => ({
    async find(ctx) {
        const { query } = ctx;

        const entity = await strapi.entityService.findMany('api::instagram-repost-page.instagram-repost-page', {
            ...query,
            populate: {
                BgImage: {
                    populate: {
                        attributes: true
                    }
                },
                Packages: {
                    populate: {
                        Image: true
                    }
                },
                Steps: {
                    populate: {
                        attributes: true
                    }
                }
            },
        });
        const sanitizedEntity = await this.sanitizeOutput(entity, ctx);

        return this.transformResponse(sanitizedEntity);

    }
}));