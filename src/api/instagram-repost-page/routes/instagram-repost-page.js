'use strict';

/**
 * instagram-repost-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::instagram-repost-page.instagram-repost-page');
