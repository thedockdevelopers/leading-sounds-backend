'use strict';

/**
 * instagram-repost-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::instagram-repost-page.instagram-repost-page');
