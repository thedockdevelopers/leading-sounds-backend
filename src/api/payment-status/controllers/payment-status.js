'use strict';
const Stripe = require('stripe');
const stripe = Stripe(process.env.STRIPE_SECRET_KEY);

module.exports = {
  index: async (ctx, next) => {
    var transactionId = ctx.request.body.transactionId;
    var charge = await stripe.charge.retrieve(transactionId).then((response) => {
      console.log(response);
      ctx.send({success: true, disputed: false});
    }).catch((error) => {
      ctx.send({success: false, disputed: false});
    })

  }
        
};