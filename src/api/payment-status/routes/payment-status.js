'use strict';

module.exports =
{
    "routes": [
        {
            "method": "POST",
            "path": "/payment-status",
            "handler": "payment-status.index"
        }
    ]
}
