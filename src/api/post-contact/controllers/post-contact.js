'use strict';

module.exports = {
  index: async (ctx, next) => {
    
    const contactObj = {
      data: {
        FirstName: ctx.request.body.clientObj.firstName,
        LastName: ctx.request.body.clientObj.lastName,
        Email: ctx.request.body.clientObj.email,
        Phone: ctx.request.body.clientObj.phone,
        Services: ctx.request.body.clientObj.services,
        AboutClient: ctx.request.body.clientObj.aboutClient,
      }
    };
    const order = await strapi.services['api::contact-querie.contact-querie'].create(contactObj).then((response) => {
      ctx.send({success: true});
    }).catch((error) => {
      ctx.send({success: false});
    })

  }
        
};