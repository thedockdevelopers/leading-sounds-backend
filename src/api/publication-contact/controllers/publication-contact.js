'use strict';

module.exports = {
  index: async (ctx, next) => {
    
    const contactObj = {
      data: {
        Name: ctx.request.body.clientObj.name,
        Phone: ctx.request.body.clientObj.phone,
        Email: ctx.request.body.clientObj.email,
        InstaUsername: ctx.request.body.clientObj.instaUsername,
        SpotifyLink: ctx.request.body.clientObj.spotifyLink,
        YoutubeChannelLink: ctx.request.body.clientObj.ytChannelLink,
        AboutClient: ctx.request.body.clientObj.aboutClient,
      }
    };
    const order = await strapi.services['api::publication-querie.publication-querie'].create(contactObj).then((response) => {
      ctx.send({success: true});
    }).catch((error) => {
      ctx.send({success: false});
    })

  }
        
};