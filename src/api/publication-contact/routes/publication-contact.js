'use strict';

module.exports =
{
    "routes": [
        {
            "method": "POST",
            "path": "/publication-contact",
            "handler": "publication-contact.index"
        }
    ]
}
