'use strict';

/**
 *  publication-package controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::publication-package.publication-package');
