'use strict';

/**
 * publication-package router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::publication-package.publication-package');
