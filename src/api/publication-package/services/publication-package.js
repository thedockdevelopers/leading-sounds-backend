'use strict';

/**
 * publication-package service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::publication-package.publication-package');
