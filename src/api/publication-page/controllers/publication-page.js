'use strict';

/**
 *  publication-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::publication-page.publication-page');
