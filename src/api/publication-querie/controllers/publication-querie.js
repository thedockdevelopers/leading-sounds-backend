'use strict';

/**
 *  publication-querie controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::publication-querie.publication-querie');
