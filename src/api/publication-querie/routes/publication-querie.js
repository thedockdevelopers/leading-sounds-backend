'use strict';

/**
 * publication-querie router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::publication-querie.publication-querie');
