'use strict';

/**
 * publication-querie service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::publication-querie.publication-querie');
