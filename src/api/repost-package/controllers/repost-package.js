'use strict';

/**
 *  repost-package controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::repost-package.repost-package');
