'use strict';

/**
 * repost-package router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::repost-package.repost-package');
