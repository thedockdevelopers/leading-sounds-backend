'use strict';

/**
 * repost-package service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::repost-package.repost-package');
