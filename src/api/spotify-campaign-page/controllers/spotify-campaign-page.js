'use strict';

/**
 *  spotify-campaign-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::spotify-campaign-page.spotify-campaign-page');
