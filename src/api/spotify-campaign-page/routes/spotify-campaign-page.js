'use strict';

/**
 * spotify-campaign-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::spotify-campaign-page.spotify-campaign-page');
