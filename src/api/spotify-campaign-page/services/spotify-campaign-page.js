'use strict';

/**
 * spotify-campaign-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::spotify-campaign-page.spotify-campaign-page');
