'use strict';

/**
 *  spotify-package controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::spotify-package.spotify-package');
