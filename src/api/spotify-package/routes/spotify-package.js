'use strict';

/**
 * spotify-package router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::spotify-package.spotify-package');
