'use strict';

/**
 * spotify-package service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::spotify-package.spotify-package');
