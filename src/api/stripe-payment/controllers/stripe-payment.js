'use strict';
const Stripe = require('stripe');
const stripe = Stripe(process.env.STRIPE_SECRET_KEY);
const { sanitizeEntity } = require('strapi-utils');
const nodemailer = require("nodemailer");

module.exports = {
  index: async (ctx, next) => {
    var name = ctx.request.body.paymentObj.userInfo.firstName + '(' + ctx.request.body.paymentObj.userInfo.email + ')';
    var couponData = ctx.request.body.paymentObj.couponData ? ctx.request.body.paymentObj.couponData : {};
    const customer = await stripe.customers.create({
      name: name,
      address: {
        line1: ctx.request.body.paymentObj.userInfo.address,
        postal_code: ctx.request.body.paymentObj.userInfo.zipCode,
        city: ctx.request.body.paymentObj.userInfo.city,
        state: ctx.request.body.paymentObj.userInfo.state,
        country: ctx.request.body.paymentObj.userInfo.country
      },
      source: ctx.request.body.paymentObj.transactionId,
    });
    const charge = await stripe.charges.create({
      amount: ctx.request.body.paymentObj.finalAmount * 100,
      currency: ctx.request.body.paymentObj.currency,
      description: ctx.request.body.paymentObj.description,
      customer: customer.id
    });
    const orderObj = {
      data: {
        CampaignType: ctx.request.body.paymentObj.category,
        CampaignData: ctx.request.body.paymentObj.cartItems,
        Amount: ctx.request.body.paymentObj.finalAmount,
        ClientData: ctx.request.body.paymentObj.userInfo,
        Currency: ctx.request.body.paymentObj.currency,
        StripeTransactionId: charge.id,
        StripeReceiptUrl: charge.receipt_url,
        CouponData: couponData
      }
    };
    const order = await strapi.services['api::campaign-order.campaign-order'].create(orderObj);

    // const order = await strapi.services['api::campaign-order.campaign-order'].create(orderObj).then((response) => {
    //   ctx.send({isSuccess: true, transactionId: charge.id});
    // }).catch((error) => {
    //   ctx.send({isSuccess: false, message: error.message});
    // });

    var transporter = await nodemailer.createTransport({
      name: 'localhost',
      host: "smtp.sendgrid.net",
      port: 465,
      secure: true, // true for 465, false for other ports
      requireTLS: true,
      auth: {
        user: process.env.SENDGRID_USER,
        pass: process.env.SENDGRID_PASS,
      },
    })
  
    // send mail with defined transport object
    var mail = await transporter.sendMail({
      from: 'amanmaheshwari055@gmail.com',
      to: ctx.request.body.paymentObj.userInfo.email,
      subject: "Leading Sounds - Payment Successful",
      html: "<div><div>Hi,</div><br><div>We have received your " + ctx.request.body.paymentObj.description + ".</div><br><div>You can access invoice by visiting " + charge.receipt_url +".</div><br><div>Thanks,</div><div>Leading Sounds Team</div></div>", // html body
    }).then((response) => {
      ctx.send({isSuccess: true, transactionId: charge.id});
    }).catch((error) => {
      ctx.send({isSuccess: false, message: error.message});
    });
  }
        
};