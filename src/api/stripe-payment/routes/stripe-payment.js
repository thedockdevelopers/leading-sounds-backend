'use strict';
const index = require('../controllers/stripe-payment')
module.exports =
{
    "routes": [
        {
            "method": "POST",
            "path": "/stripe-payment",
            "handler": "stripe-payment.index"
        }
    ]
}
