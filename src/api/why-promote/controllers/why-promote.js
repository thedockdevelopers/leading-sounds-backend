'use strict';

/**
 *  why-promote controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::why-promote.why-promote');
