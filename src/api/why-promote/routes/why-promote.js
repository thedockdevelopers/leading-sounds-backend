'use strict';

/**
 * why-promote router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::why-promote.why-promote');
