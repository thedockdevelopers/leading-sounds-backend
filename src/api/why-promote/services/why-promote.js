'use strict';

/**
 * why-promote service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::why-promote.why-promote');
