'use strict';

/**
 *  youtube-campaign-page controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::youtube-campaign-page.youtube-campaign-page');
