'use strict';

/**
 * youtube-campaign-page router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::youtube-campaign-page.youtube-campaign-page');
