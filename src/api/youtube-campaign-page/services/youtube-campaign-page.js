'use strict';

/**
 * youtube-campaign-page service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::youtube-campaign-page.youtube-campaign-page');
