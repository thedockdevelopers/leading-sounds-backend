'use strict';

/**
 *  youtube-package controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::youtube-package.youtube-package');
