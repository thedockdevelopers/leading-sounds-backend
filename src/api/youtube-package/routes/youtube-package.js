'use strict';

/**
 * youtube-package router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::youtube-package.youtube-package');
