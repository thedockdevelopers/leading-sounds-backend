'use strict';

/**
 * youtube-package service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::youtube-package.youtube-package');
